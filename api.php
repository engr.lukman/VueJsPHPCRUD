<?php

$conn = new mysqli("localhost", "root", "", "vueapps");
 
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$out = array('error' => false);

$crud = 'read';

if(isset($_GET['crud'])){
	$crud = $_GET['crud'];
}


if($crud == 'read'){
	$sql = "select * from contacts";
	$query = $conn->query($sql);
	$result = array();

	while($row = $query->fetch_array()){
		array_push($result, $row);
	}

	$out['contacts'] = $result;
}

if($crud == 'create'){
	$name = $_POST['name'];
	$mobile = $_POST['mobile'];
	$email = $_POST['email'];
	$sql = "insert into contacts (name, mobile, email) values ('$name', '$mobile', '$email')";
	$query = $conn->query($sql);

	if($query){
		$out['message'] = "Contact Added Successfully";
	}
	else{
		$out['error'] = true;
		$out['message'] = "Could not add Contact";
	}	
}

if($crud == 'update'){
	$id = $_POST['id'];
	$name = $_POST['name'];
	$mobile = $_POST['mobile'];
	$email = $_POST['email'];

	$sql = "update contacts set name='$name', mobile='$mobile', email='$email' where id='$id'";
	$query = $conn->query($sql);

	if($query){
		$out['message'] = "Contact Updated Successfully";
	}
	else{
		$out['error'] = true;
		$out['message'] = "Could not update Contact";
	}
	
}

if($crud == 'delete'){

	$id = $_POST['id'];

	$sql = "delete from contacts where id='$id'";
	$query = $conn->query($sql);

	if($query){
		$out['message'] = "Contact Deleted Successfully";
	}
	else{
		$out['error'] = true;
		$out['message'] = "Could not delete Contact";
	}
	
}


$conn->close();

header("Content-type: application/json");
echo json_encode($out);
die();


?>