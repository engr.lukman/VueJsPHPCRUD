var app = new Vue({
	el: '#contacts',
	data:{
		showAddModal: false,
		showEditModal: false,
		showDeleteModal: false,
		errorMessage: "",
		successMessage: "",
		contacts: [],
		newContact: {name: '', mobile: '', email: ''},
		clickContact: {}
	},

	mounted: function(){
		this.getAllContacts();
	},

	methods:{
		getAllContacts: function(){
			axios.get('api.php')
				.then(function(response){
					if(response.data.error){
						app.errorMessage = response.data.message;
					}
					else{
						app.contacts = response.data.contacts;
					}
				});
		},

		saveContact: function(){
			var myForm = app.toFormData(app.newContact);
			axios.post('api.php?crud=create', myForm)
				.then(function(response){
					app.newContact = {name: '', mobile:'', email:''};
					if(response.data.error){
						app.errorMessage = response.data.message;
					}
					else{
						app.successMessage = response.data.message
						app.getAllContacts();
					}
				});
		},

		updateContact(){
			var myForm = app.toFormData(app.clickContact);
			axios.post('api.php?crud=update', myForm)
				.then(function(response){
					app.clickContact = {};
					if(response.data.error){
						app.errorMessage = response.data.message;
					}
					else{
						app.successMessage = response.data.message
						app.getAllContacts();
					}
				});
		},

		deleteContact(){
			var myForm = app.toFormData(app.clickContact);
			axios.post('api.php?crud=delete', myForm)
				.then(function(response){
					app.clickContact = {};
					if(response.data.error){
						app.errorMessage = response.data.message;
					}
					else{
						app.successMessage = response.data.message
						app.getAllContacts();
					}
				});
		},

		selectContact(contact){
			app.clickContact = contact;
		},

		toFormData: function(obj){
			var form_data = new FormData();
			for(var key in obj){
				form_data.append(key, obj[key]);
			}
			return form_data;
		},

		clearMessage: function(){
			app.errorMessage = '';
			app.successMessage = '';
		}

	}
});