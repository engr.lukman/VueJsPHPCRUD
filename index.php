<!DOCTYPE html>
<html>
<head>
	<title>Vue.js CRUD using PHP/MySQLi</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="container">
	<h1 class="page-header text-center">Vue.js CRUD with PHP/MySQLi</h1>
	<div id="contacts">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-md-12">
					<h2>Contact List
					<button class="btn btn-primary pull-right" @click="showAddModal = true"><span class="glyphicon glyphicon-plus"></span> Contact</button>
					</h2>
				</div>
			</div>

			<div class="alert alert-danger text-center" v-if="errorMessage">
				<button type="button" class="close" @click="clearMessage();"><span aria-hidden="true">&times;</span></button>
				<span class="glyphicon glyphicon-alert"></span> {{ errorMessage }}
			</div>
			
			<div class="alert alert-success text-center" v-if="successMessage">
				<button type="button" class="close" @click="clearMessage();"><span aria-hidden="true">&times;</span></button>
				<span class="glyphicon glyphicon-ok"></span> {{ successMessage }}
			</div>

			<table class="table table-bordered table-striped">
				<thead>
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<th>Action</th>
				</thead>
				<tbody>
					<tr v-for="contact in contacts">
						<td>{{ contact.name }}</td>
						<td>{{ contact.mobile }}</td>
						<td>{{ contact.email }}</td>
						<td>
							<button class="btn btn-success" @click="showEditModal = true; selectContact(contact);"><span class="glyphicon glyphicon-edit"></span> Edit</button> 
							<button class="btn btn-danger" @click="showDeleteModal = true; selectContact(contact);"><span class="glyphicon glyphicon-trash"></span> Delete</button>

						</td>
					</tr>
				</tbody>
			</table>
		</div>

		
		
		<!-- Add Modal -->
		<div class="myModal" v-if="showAddModal">
			<div class="modalContainer">
				<div class="modalHeader">
					<span class="headerTitle">Add New Contact</span>
					<button class="closeBtn pull-right" @click="showAddModal = false">&times;</button>
				</div>
				<div class="modalBody">
					<div class="form-group">
						<label>Name:</label>
						<input type="text" class="form-control" v-model="newContact.name">
					</div>
					<div class="form-group">
						<label>Mobile:</label>
						<input type="text" class="form-control" v-model="newContact.mobile">
					</div>
					<div class="form-group">
						<label>Email:</label>
						<input type="text" class="form-control" v-model="newContact.email">
					</div>
				</div>
				<hr>
				<div class="modalFooter">
					<div class="footerBtn pull-right">
						<button class="btn btn-default" @click="showAddModal = false"><span class="glyphicon glyphicon-remove"></span> Cancel</button> 
						<button class="btn btn-primary" @click="showAddModal = false; saveContact();"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Edit Modal -->
		<div class="myModal" v-if="showEditModal">
			<div class="modalContainer">
				<div class="editHeader">
					<span class="headerTitle">Edit Contact</span>
					<button class="closeEditBtn pull-right" @click="showEditModal = false">&times;</button>
				</div>
				<div class="modalBody">
					<div class="form-group">
						<label>Name:</label>
						<input type="text" class="form-control" v-model="clickContact.name">
					</div>
					<div class="form-group">
						<label>Mobile:</label>
						<input type="text" class="form-control" v-model="clickContact.mobile">
					</div>
					<div class="form-group">
						<label>Email:</label>
						<input type="text" class="form-control" v-model="clickContact.email">
					</div>
				</div>
				<hr>
				<div class="modalFooter">
					<div class="footerBtn pull-right">
						<button class="btn btn-default" @click="showEditModal = false"><span class="glyphicon glyphicon-remove"></span> Cancel</button> 
						<button class="btn btn-success" @click="showEditModal = false; updateContact();"><span class="glyphicon glyphicon-check"></span> Save</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Delete Modal -->
		<div class="myModal" v-if="showDeleteModal">
			<div class="modalContainer">
				<div class="deleteHeader">
					<span class="headerTitle">Delete Contact</span>
					<button class="closeDelBtn pull-right" @click="showDeleteModal = false">&times;</button>
				</div>
				<div class="modalBody">
					<h5 class="text-center">Are you sure you want to Delete this record.</h5>
				</div>
				<hr>
				<div class="modalFooter">
					<div class="footerBtn pull-right">
						<button class="btn btn-default" @click="showDeleteModal = false"><span class="glyphicon glyphicon-remove"></span> Cancel</button> 
						<button class="btn btn-danger" @click="showDeleteModal = false; deleteContact(); "><span class="glyphicon glyphicon-trash"></span> Yes</button>
					</div>
				</div>
			</div>
		</div>

		
		
	</div>
</div>
<script src="vue.js"></script>
<script src="axios.js"></script>
<script src="app.js"></script>
</body>
</html>